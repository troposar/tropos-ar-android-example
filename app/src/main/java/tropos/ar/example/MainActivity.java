package tropos.ar.example;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import tropos.ar.TAR;
import tropos.ar.TARError;
import tropos.ar.TroposAR;


public class MainActivity extends Activity {
    TAR oTar = new TAR();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /** Called when the user touches the button */
    public void btnStartClicked(View view) {
        try {
            //Show the Tropos AR view and directly open an AR asset
            oTar.ShowTAR(this, this, "6193861cbac025c7b87c43ad");
            //Show the Tropos AR view
            oTar.ShowTAR(this, this);

        } catch (TARError tarError) {
            Log.d("TroposSDK", String.valueOf(tarError.getType()));
        }
    }
}